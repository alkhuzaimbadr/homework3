// 2. What are the different types of exceptions?

const { add1,
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject } = require("./lib.js");


function main() {
    try {
        // we can throw numbers.
        throw 3;
    } catch (e) {
        console.log(e);
    }


    try {
        // we can throw strings.
        throw "hi";
    } catch (e) {
        console.log(e);
    }


    try {
        // we can throw objects.
        throw { "field1": "value1", "field2": true};
    } catch (e) {
        console.log(e);
    }


    try {
        // we can throw booleans.
        throw true;
    } catch (e) {
        console.log(e);
    }


    try {
        // we can throw instances of Error.
        throw new Error("I'm an error");
    } catch (e) {

        // Notice an Error object captures a stack trace when you raise it
        // and prints it when you print the Error.
        console.log(e);
    }
    console.log("Done");
}

main();